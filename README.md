## Installation ##

* Copy over everything to ```$CONFIG_DIR/Packages/User```. ```$CONFIG_DIR``` is typically ```~/.config/sublime-text-3/```

## What is this? ##

A sublime plugin that makes Sublime behave like emacs. It consists of a **emacs_commands.py** that contains functions for starting a selection *(Ctrl-Space)* in emacs, killing a line, and others.
The **sublime-keymap** file binds the functions to keyboard shortcuts and defines other shortcuts for emacs-like navigation.

## Licence ##
None! Do whatever you please!
