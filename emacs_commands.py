import sublime, sublime_plugin, copy

CHECK_SELECTION = True

text_selected = lambda view:\
	CHECK_SELECTION and any(region.size() > 0 for region in view.sel())

# ctrl-space in emacs sets mark
mark_is_set = False

class RemoveSelectionCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		# It is undocumented and there are selectors .begin(), .end()
		# but the cursor seems to be placed at '.b'
		# The doc:
		# https://www.sublimetext.com/docs/2/api_reference.html#sublime.Region
		old_sel = [sublime.Region(x.b, x.b) for x in self.view.sel()]
		
		self.view.sel().clear()
		for region in old_sel:
			self.view.sel().add(region)




class ToggleMarkCommand(sublime_plugin.TextCommand):
	"""Toggles mark, clears selection. 

	Leaves cursors positions.
	"""
	def run(self, edit):
		global mark_is_set

		self.view.run_command("remove_selection")
		mark_is_set = not mark_is_set

class MoveSelectionAwareCommand(sublime_plugin.TextCommand):
	def run(self, edit, **kwargs):
		print(kwargs)
		kwargs['extend'] = text_selected(self.view) or mark_is_set
		self.view.run_command("move", kwargs)

class MoveToSelectionAwareCommand(sublime_plugin.TextCommand):
	def run(self, edit, **kwargs):
		print(kwargs)
		kwargs['extend'] = text_selected(self.view) or mark_is_set
		self.view.run_command("move_to", kwargs)



class KillLineCommand(sublime_plugin.TextCommand):
	"""TODO: If already at end of line, remove endline"""
	def run(self, edit):
		view = self.view

		# Moves from current position to end of line. Extends selection from current position to oel
		# Behaves funny with multiple selections: all selections are extended to corresponding eol
		view.run_command("move_to", {"to": "eol", "extend": True})

		# print(view.sel)

		# Replaces (all)selection(s) with ""
		view.run_command("insert", {"characters": ""})


class KillSelectionCommand(sublime_plugin.TextCommand):
	"""TODO: If already at end of line, remove endline"""
	def run(self, edit):
		view = self.view
		view.run_command("copy")
		view.run_command("insert", {"characters": ""})

